import difflib
import unicodedata


class Comparer():
    '''
    Receber a palavra limpa (somente o nome do produto) com array de todos produtos e retornar as possibilidades.
    '''
    @staticmethod
    def array_possibilities(product, list_to_compare):

        dry_list = [key for key in list_to_compare.keys() if difflib.SequenceMatcher(
            None, product.split(" ")[0], key[:15]).ratio() > 0.3]

        def ordering(elem):
            return difflib.SequenceMatcher(None, product, elem[:15]).ratio()
        dry_list.sort(key=ordering, reverse=True)

        first_filter = [i for i in dry_list if unicodedata.normalize(
            'NFKD', product).encode('ASCII', 'ignore').decode()[:2] in unicodedata.normalize(
            'NFKD', i).encode('ASCII', 'ignore').decode()]

        set_list = set(product.split(" "))

        more_accurate = []
        for item in first_filter:
            c_it = set(item.split(" "))
            inter = set_list.intersection(c_it)
            if len(set_list) == len(inter):
                more_accurate.append(item)

        if not more_accurate:
            more_accurate = first_filter

        return more_accurate
