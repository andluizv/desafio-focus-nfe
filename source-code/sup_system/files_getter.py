from tkinter import filedialog, messagebox

from sup_system.exceptions import FileSkipped


class FilesGetter():
    @staticmethod
    def get_txt_file():
        messagebox.showinfo(title='Arquivo texto whatsapp',
                            message='Primeiro escolha o arquivo com as mensagens do WhatsApp:')
        txt_file = filedialog.askopenfile(filetypes=[('Text File', '*.txt')])
        if not txt_file:
            raise FileSkipped

        return txt_file

    @staticmethod
    def get_prices_file():
        messagebox.showinfo(title='Arquivo com tablea de preços',
                            message='Agora escolha o arquivo que contém os preços: (pdf pode demorar um pouco, aguarde!')

        prices_file = filedialog.askopenfilename(
            filetypes=[('Planilha excel', "*.xlsx *.xls"), ('PDF', '*.pdf')])

        if not prices_file:
            raise FileSkipped

        return prices_file
