from camelot import read_pdf
import pandas as pd
from tkinter import messagebox, filedialog
from datetime import datetime
from sup_system.exceptions import FileSkipped, TxtBlocksError


class DictGenerator():

    @staticmethod
    def prices_from_pdf(file_path):

        file_decoded = read_pdf(file_path, pages='1-end')
        output = {}
        unitis = ["kg", "un", "g", "gr", "maço", "dz",
                  "und", "unidade", 'haste', "aprox", "ml"]

        for page in file_decoded:
            for item in page.df.to_dict('split')['data']:
                key = "".join([letter for letter in item[1] if letter.isalpha(
                ) or letter == " "]).lower().strip()
                clean_key = " ".join(
                    [word for word in key.split(" ") if word not in unitis]).strip()
                output[clean_key] = {'supplier': item[0],
                                     'price': item[2], "original_name": item[1]}

        output.pop('descrição')
        return output

    @staticmethod
    def prices_from_excel(file_path):

        file_decoded = pd.read_excel(file_path)
        output = {}

        for item in file_decoded.to_dict('split')['data']:
            output["".join([letter for letter in item[1] if letter.isalpha(
            ) or letter == " "]).lower().strip()] = {'supplier': item[0], 'price': item[2], "original_name": item[1]}

        return output

    @staticmethod
    def order(file):
        blocks = ("|".join(file.read().splitlines())).split('||')
        if blocks[0][0] == "|":
            raise TxtBlocksError
        orders = [block.split('|') for block in blocks]
        orders_dict = dict([("".join([letter for letter in item[0] if letter.isalpha(
        ) or letter == " "]).strip(), item[1:]) for item in orders])
        return orders_dict


class ExcelFileGenerator():

    @staticmethod
    def save(data):
        file_name = datetime.now().strftime("%d-%b-%y-%H-%M")
        columns = [
            'Nome', 'Produto pedido', 'Produto encontrado', 'Fornecedor', 'Quantidade', 'Valor unitário', 'Valor total']
        df = pd.DataFrame(data, columns=columns)

        messagebox.showinfo(title='Pasta de saída', message='Escolha uma pasta onde irá salvar a tabela.')
        folder = filedialog.askdirectory()
        if not folder:
            raise FileSkipped

        writer = pd.ExcelWriter(f'{folder}/{file_name}.xlsx', engine='xlsxwriter')
        df.to_excel(writer, sheet_name='result', index=False)

        workbook = writer.book
        worksheet = writer.sheets['result']

        f_centered = workbook.add_format({'align': 'center'})
        f_not_found = workbook.add_format({'bg_color': '#ff3838'})
        f_verify = workbook.add_format({'bg_color': '#ffd428'})

        worksheet.set_column('A:A', 20)
        worksheet.set_column('B:C', 40)
        worksheet.set_column('D:D', 20, f_centered)
        worksheet.set_column('E:G', 15, f_centered)

        worksheet.conditional_format('C1:C999', {
            'type': 'cell', 'criteria': 'equal to', 'value': '"Sem match"', 'format': f_not_found})
        worksheet.conditional_format('C2:E999', {
            'type': 'text', 'criteria': 'containing', 'value': '?', 'format': f_verify})

        writer.save()
        messagebox.showwarning(title='Planilha criada',
                               message=f'A planilha foi criada e salva com o nome de {file_name}.xlsx')
