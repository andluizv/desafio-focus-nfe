import tkinter as tk
from tkinter import messagebox
import difflib
import re

from sup_system.comparer import Comparer
from sup_system.exceptions import FileSkipped, TxtBlocksError
from sup_system.files_getter import FilesGetter
from sup_system.generators import DictGenerator, ExcelFileGenerator

'''
TODO:
    - RECONSTRUIR O COMPARADOR DE NOMES;
    - SISTEMA LOADING PRO PDF;
    - USAR PRODUTO COM MENOR PREÇO;
'''

root = tk.Tk()
root.withdraw()


def program():
    try:
        txt_file = FilesGetter.get_txt_file()
        orders_dict = DictGenerator.order(txt_file)

        prices_file = FilesGetter.get_prices_file()
        extension = prices_file[-3:]

        print("Lendo tabela de preços...", end=' ', flush=True)
        if extension == 'pdf':
            prices_dict = DictGenerator.prices_from_pdf(prices_file)
        else:
            prices_dict = DictGenerator.prices_from_excel(prices_file)
        print('Pronto!')

        to_excel = []
        unnecessary = ["kg", "kl", "un", "g", "gr", "maço", "maços", "dz",
                       "und", "unidade", 'haste', "aprox", "ml", "k", "duz", "duzias", "dúzias", "duzia", 'kh', "grs", "mc", "de"]

        for person, items in orders_dict.items():
            for product in items:

                clean_name = "".join([letter for letter in product if letter.isalpha(
                ) or letter == " "]).lower().strip()
                prod_name = " ".join(
                    [word for word in clean_name.split(" ") if word not in unnecessary])

                matches_list = Comparer.array_possibilities(
                    prod_name, prices_dict)
                original_name = prices_dict[matches_list[0]]['original_name'] if matches_list else 'Sem match'
                if not matches_list:
                    price, quantity = 0, 0
                    product_from_list = "Sem match"
                    supplier = ""
                    multiple = 0
                else:
                    product_from_list = matches_list[0]
                    price = float(prices_dict[product_from_list]
                                  ['price'].replace(",", "."))
                    quantity = product.lower().replace(prod_name, "")
                    nums = re.findall('[0-9]+', quantity)
                    supplier = prices_dict[product_from_list]['supplier']

                    # corrige os 1/2
                    if "/" in quantity:
                        if len(nums) == 2:
                            nums[0] = 0.5
                        else:
                            nums[0] = int(nums[0]) + 0.5

                    multiple = int(nums[0]) if nums else 1

                    # unidades diferentes, faz marcação na tablea
                    for elem in ['gr', 'grs', 'ml']:
                        if elem in quantity:
                            quantity, multiple = quantity + "?", 0

                    # caso exista muita diferença ou muitas possibilidades, faz marcação na tabela
                    if difflib.SequenceMatcher(None, product_from_list, prod_name).ratio() < 0.31 or len(matches_list) > 3:
                        product_from_list = original_name + "?"
                    else:
                        product_from_list = original_name

                to_excel.append([person, prod_name, product_from_list,
                                supplier, quantity, price, multiple*price])

        ExcelFileGenerator.save(to_excel)

    except FileSkipped:
        messagebox.showerror(title="Arquivo não selecionado",
                             message="Etapa cancelada, o programa irá abortar a operação.")
    except TxtBlocksError:
        messagebox.showerror(title='Erro no txt', message='Erro nos blocos do txt, verifique se o arquivo não está iniciando com "enter"')


if __name__ == '__main__':
    program()
